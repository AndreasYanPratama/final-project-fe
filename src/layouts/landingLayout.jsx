import { createContext, useContext, useState } from 'react';
import NavbarComponent from '@/components/navbar'
import FooterComponent from '@/components/footer'

const LandingLayoutContext = createContext();

export const useLandingLayoutContext = () => useContext(LandingLayoutContext);

export default function LandingLayout({ children }) {
    const [data, setData] = useState();
    return (
        <LandingLayoutContext.Provider value={{
            data,
            setData,
          }}
          >
            <div className="text-gray-700 bg-white">
              <NavbarComponent />
              {children}
              <FooterComponent />
            </div>
        </LandingLayoutContext.Provider>
    );
}
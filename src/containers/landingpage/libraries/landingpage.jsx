export default function LandingPage() {
  return(
  <>
      <div className="pt-60 pb-20 bg-white ">
        <div className="container mx-auto px-6 text-center">
          <div className="text-4xl font-bold mb-2 text-black">
            Get Bussiness Loan the Smart Way
          </div>
          <div className="text-2xl mb-8 text-gray-800">
            Get additional funds for your bussiness the easy way. It&apos;s free and won&apos;t affect your credit score
          </div>
          <button type="button" className="transform hover:scale-110 transition duration-300 ease-in-out bg-blue-700 text-white font-bold rounded py-3 px-8 shadow-lg tracking-wider">
            Get Started &#8594;
          </button>
        </div>
      </div>
      <section className="container py-10">
        <div className="mb-10 w-full md:w-full lg:w-full flex justify-center items-center ">
          <img src="https://cdn.dribbble.com/users/2487395/screenshots/6311129/responsivedesign_2x.png" alt="responsive" />
        </div>
      </section>
      <section className="bg-white">
        <div className="container mx-auto px-6 py-20">
          <h2 className="text-4xl font-bold text-center text-gray-800 mb-8">
            How Can We Help ?
          </h2>
          <div className="flex justify-center items-center">
            <div className="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 gap-5 ">
              <div className="bg-gray-100 rounded overflow-hidden shadow-lg">
                <div className="px-6 py-4">
                  <div className="font-bold text-xl mb-2">Borrower</div>
                  <p className="text-gray-700 text-sm">
                    We provide productive financing solution with many benefits as borrower
                  </p>
                </div>
                <div className="grid grid-cols-4 px-6 mb-4">
                      <img className="h-20" src="https://cdn.dribbble.com/users/2487395/screenshots/6311129/responsivedesign_2x.png" alt="Mountain"/>
                      <p className="col-span-3 text-gray-500text-xs flex justify-start items-center md:text-sm">Fast & easy process</p> 
                    </div>
                    <div className="grid grid-cols-4 px-6 mb-4">
                      <img className="h-20" src="https://cdn.dribbble.com/users/2487395/screenshots/6311129/responsivedesign_2x.png" alt="Mountain"/>
                      <p className="col-span-3 text-gray-500text-xs flex justify-start items-center md:text-sm">Transparent</p> 
                    </div>
                <div className="px-6 pt-4 pb-2">
                  <button className="inline-block bg-blue-700 rounded px-4 py-3 text-sm font-semibold text-white mr-2 mb-2 border border-gray-500 hover:bg-blue-300">
                    Fund a Loan
                  </button>
                  <button className="inline-block bg-white rounded px-4 py-3 text-sm font-semibold text-blue-700 mr-2 mb-2 border border-gray-500 hover:bg-blue-300">
                    Learn More
                  </button>
                </div>
              </div>
              <div className="bg-gray-100 rounded overflow-hidden shadow-lg">
                <div className="px-6 py-4">
                  <div className="font-bold text-xl mb-2">Lender</div>
                  <p className="text-gray-700 text-sm">
                    Empowering startups by helping theirs bussinesses
                  </p>
                </div>
                <div className="grid grid-cols-4 px-6 mb-4">
                  <img className="h-20" src="https://cdn.dribbble.com/users/2487395/screenshots/6311129/responsivedesign_2x.png" alt="Mountain"/>
                  <p className="col-span-3 text-gray-500text-xs flex justify-start items-center md:text-sm">Fast & easy process</p> 
                </div>
                <div className="grid grid-cols-4 px-6 mb-4">
                  <img className="h-20" src="https://cdn.dribbble.com/users/2487395/screenshots/6311129/responsivedesign_2x.png" alt="Mountain"/>
                  <p className="col-span-3 text-gray-500text-xs flex justify-start items-center md:text-sm">Transparent</p> 
                </div>
                <div className="px-6 pt-4 pb-2">
                  <button className="inline-block bg-blue-700 rounded px-4 py-3 text-sm font-semibold text-white mr-2 mb-2 border border-gray-500 hover:bg-blue-300">
                    Fund a Loan
                  </button>
                  <button className="inline-block bg-white rounded px-4 py-3 text-sm font-semibold text-blue-700 mr-2 mb-2 border border-gray-500 hover:bg-blue-300">
                    Learn More
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="bg-white">
        <div className="container mx-auto px-6 text-center py-10 mb-5">
          <h2 className="mb-6 text-4xl font-bold text-center text-black">
            Why clients love us
          </h2>
          <div className="flex justify-center items-center mb-6">
            <img className="inline object-cover w-48 h-48 mr-2 rounded-full" src="https://randomuser.me/api/portraits/women/81.jpg" alt="Profile image"/>
          </div>
          <p className="text-gray-500 text-base px-6 mb-5">
            Sunt corrupti delectus eaque pariatur dicta magnam,
            velit possimus cupiditate iusto hic,
            ullam, error vel odio adipisci!
            Mollitia molestias sit beatae? Corrupti!
          </p>
          <p className="text-gray-800 text-xs md:text-sm px-6">
            <span className="font-bold">Jane Doe</span> - Founder and CEO of Sunday
          </p>
        </div>
        <div className="container mx-auto px-6 text-center py-10">
          <h2 className="mb-3 text-4xl font-bold text-center text-black">
            Licensed and Supervised by
          </h2>
          <div className="flex justify-center items-center">
            <div className="mb-3 md:w-1/3 lg:w-1/3 flex justify-center items-center ">
              <img src="https://1.bp.blogspot.com/-3LFFnTsKzLk/YHMwZkH4KXI/AAAAAAAACaw/lA2gmRFu7m84iRdR4597XqBw_MusjB6gQCNcBGAsYHQ/s320/Otoritas%2BJasa%2BKeuangan.png" alt="responsive" />
            </div>
          </div>
        </div>
      </section>
  </>
  );
}
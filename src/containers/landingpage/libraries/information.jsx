export default function InformationPage() {
    return(
        <section className="bg-white">
            <div className="container mx-auto px-6 py-20">
            <h2 className="text-4xl font-bold text-center text-gray-800 mb-8">
                Information
            </h2>
            <div className="flex justify-center items-center">
                <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Donec quis nisl eu dolor eleifend tincidunt. 
                Duis aliquam feugiat erat. 
                Class aptent taciti sociosqu ad litora torquent per conubia nostra, 
                per inceptos himenaeos. 
                Mauris fermentum nulla vitae dui tempor, 
                ut venenatis nunc condimentum. 
                Duis sit amet tellus consectetur, blandit quam sed, malesuada lorem. 
                Pellentesque vehicula diam velit, vitae tincidunt massa vestibulum at. 
                Vivamus quis fringilla dolor. 
                Maecenas cursus massa eget neque rhoncus feugiat. 
                Sed porta id felis vehicula bibendum. 
                Aliquam sit amet dapibus lorem. 
                Donec ornare et ex et efficitur. 
                Nulla iaculis dignissim luctus. 
                Donec ultrices efficitur consequat. 
                Sed eget tellus hendrerit, posuere sem vitae, dignissim massa.
                </p>
            </div>
            </div>
        </section>
    );
}
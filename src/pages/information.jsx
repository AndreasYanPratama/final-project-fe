import Head from 'next/head'
// import styles from '../styles/Home.module.css'
import LandingLayout from '@/layouts/landingLayout';
import { InformationPage} from '@/containers/landingpage/';

export default function Information() {
    return (
      <>
      <Head>
        <title>FundRaisey - Information</title>
      </Head>
      <LandingLayout>
        <InformationPage/>
      </LandingLayout>
      </>
    );
  }
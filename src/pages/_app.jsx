// import { Provider } from 'react-redux';
// import store from '@/redux/store';

import '../styles/globals.css'; // tailwind
import '../styles/app.css'; // custom css -> menambahkan font
import '../styles/landing.css';

function MyApp({ Component, pageProps }) {
  // return (
  //   <Provider store={store}>
  //     <div>
  //       {/* Dari App */}
  //     </div>
  //     <Component {...pageProps} />
  //   </Provider>
  // );
  return <Component {...pageProps} />
}

export default MyApp;

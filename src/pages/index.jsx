import Head from 'next/head'
// import styles from '../styles/Home.module.css'
import LandingLayout from '@/layouts/landingLayout';
import { LandingPage } from '@/containers/landingpage/';

export default function Home() {
  return (
    <>
    <Head>
      <title>FundRaisey</title>
    </Head>
    <LandingLayout>
      <LandingPage/>
    </LandingLayout>
    </>
  );
}

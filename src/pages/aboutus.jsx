import Head from 'next/head'
// import styles from '../styles/Home.module.css'
import LandingLayout from '@/layouts/landingLayout';
import { AboutUsPage } from '@/containers/landingpage/';

export default function Information() {
    return (
      <>
      <Head>
        <title>FundRaisey - About Us</title>
      </Head>
      <LandingLayout>
        <AboutUsPage/>
      </LandingLayout>
      </>
    );
}
import { useState } from 'react';
import Link from 'next/link';

export default function Navbar({ fixed }) {
  const [navbarOpen, setNavbarOpen] = useState(false);
  return (
    <>
      <nav className="fixed w-full z-10 flex flex-wrap items-center justify-between px-2 py-2 bg-white ">
        <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <Link href="/" className="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase">
              <a>
              {/* eslint-disable-next-line @next/next/no-img-element */}
              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYctjnEm0Z5N3lxQDGY_0LoxVesPwu2X-7mA&usqp=CAU" width="50" height="50" alt="" />
              </a>
            </Link>
            <button
              className="text-white cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent border-gray-400 rounded bg-transparent block lg:hidden outline-none focus:outline-none"
              type="button"
              onClick={() => setNavbarOpen(!navbarOpen)}
            >
              <svg
                className="fill-current h-3 w-3 bg-gray-500"
                viewBox="0 0 20 20"
                xmlns="http://wwww.w3.org/2000/svg"
              >
                <title>Menu</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
              </svg>
            </button>
          </div>
          <div
            className={
              `lg:flex flex-grow items-center${
                navbarOpen ? ' flex' : ' hidden'}`
            }
            id="example-navbar-danger"
          >
            <ul className="flex flex-col lg:flex-row list-none lg:ml-auto flex justify-center items-center">
              <li className="nav-item">
                <Link href="/information" className="ml-2">
                  <a className="px-3 py-4 flex items-center text-xs font-bold leading-snug text-gray-500 hover:opacity-75">
                    Information
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/aboutus" className="ml-2">
                  <a className="px-3 py-4 flex items-center text-xs font-bold leading-snug text-gray-500 hover:opacity-75">
                    About Us
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                  <Link href="/" className="ml-2">
                    <a className="px-3 py-4 flex items-center text-xs font-bold leading-snug text-gray-500 hover:opacity-75">
                      <button type="button" className="bg-white border border-blue-500 rounded px-5 py-2 hover:bg-gray-500 hover:text-white">
                        Login as Borrower
                      </button>
                    </a>
                  </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
